/**
 * Magebit_ConfigurablePrices
 *
 * @category     Magebit
 * @package      Magebit_ConfigurablePrices
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2015 Magebit, Ltd.(http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
Product.Config.prototype.getOptionLabel = function(option, price){
	var price = parseFloat(price);
	var priceBase = parseFloat(this.config.basePrice);

	if (this.taxConfig.includeTax) {
		var tax = price / (100 + this.taxConfig.defaultTax) * this.taxConfig.defaultTax;
		var excl = price - tax;
		var incl = excl*(1+(this.taxConfig.currentTax/100));
		var taxBase = priceBase / (100 + this.taxConfig.defaultTax) * this.taxConfig.defaultTax;
		var exclBase = priceBase - taxBase;
		var inclBase = exclBase*(1+(this.taxConfig.currentTax/100));
	} else {
		var tax = price * (this.taxConfig.currentTax / 100);
		var excl = price;
		var incl = excl + tax;
		var taxBase = priceBase * (this.taxConfig.currentTax / 100);
		var exclBase = priceBase;
		var inclBase = exclBase + taxBase;
	}

	excl += exclBase;
	incl += inclBase;

	if (this.taxConfig.showIncludeTax || this.taxConfig.showBothPrices) {
		price = incl;
	} else {
		price = excl;
	}

	var str = option.label;
	if(price){
		if (this.taxConfig.showBothPrices) {
			str+= ' ' + this.formatPrice(excl, false) + ' (' + this.formatPrice(price, false) + ' ' + this.taxConfig.inclTaxTitle + ')';
		} else {
			str+= ' ' + this.formatPrice(price, false);
		}
	}
	return str;
};

Product.Config.prototype.reloadOptionLabels = function(element){
	for(var i=0;i<element.options.length;i++){
		if(element.options[i].config){
			element.options[i].text = this.getOptionLabel(element.options[i].config, element.options[i].config.price);
		}
	}
}