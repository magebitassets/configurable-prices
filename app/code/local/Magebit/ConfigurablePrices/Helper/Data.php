<?php

/**
 * Magebit_ConfigurablePrices
 *
 * @category     Magebit
 * @package      Magebit_ConfigurablePrices
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2015 Magebit, Ltd.(http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_ConfigurablePrices_Helper_Data extends Mage_Core_Helper_Abstract
{

}